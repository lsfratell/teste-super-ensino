# Teste Super Ensino
---

## Como rodar
No windows, abra o powershell no diretório do projeto e digite:

1. Navegue até o diretório do projeto.
2. Crie uma venv `python -m venv venv`
3. Ative a venv `.\venv\Scripts\activate`
4. Instale as dependências `pip install -r .\requirements.txt`
5. Seta as variáveis de ambiente `$env:QUART_APP="main:app"`
6. Inicie o banco de dados `quart init_db`
7. Rode a aplicação `quart run`
8. Rode os tests `pytest tests -v`
