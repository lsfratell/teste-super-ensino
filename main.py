from quart import Quart, redirect, url_for, session, g
from models import db_proxy, SqliteDatabase, User, School, DoesNotExist
from modules import bp_school, bp_user, bp_session

app = Quart(__name__)

app.config.update({
    "SECRET_KEY": "development",
    "DATABASE": "database.db3"
})

db = SqliteDatabase(app.config["DATABASE"])
db_proxy.initialize(db)


app.register_blueprint(bp_school, url_prefix="/school")
app.register_blueprint(bp_session, url_prefix="/session")
app.register_blueprint(bp_user, url_prefix="/user")


"""
    Comando para criar o banco de dados e as tabelas.
"""
@app.cli.command("init_db")
def init_db():
    _init_db()


def _init_db():
    with db:
        db.create_tables([School, User])


"""
    Carrega o usuário no objeto global para estar disponivel em todo
    contexto da requisição.
"""
@app.before_request
async def before_request():
    try:
        if "logged" in session.keys():
            g.user = User.get_by_id(session.get("user_id"))
    except DoesNotExist:
        session.clear()
        return redirect(url_for("session.create"))


"""
    Essa rota verifica se o usuário está logado,
    se não, redireciona para o login, caso contrario redireciona
    para home do usuário.
"""
@app.route("/", methods=["GET"])
async def index():
    if not hasattr(g, "user"):
        return redirect(url_for("session.create"))
    return redirect(url_for("user.profile"))
