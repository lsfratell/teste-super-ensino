import bcrypt

"""
    Um função simples para validar os formulários.
"""
def validate_form(form):
    for k in form.keys():
        if k == "is_admin":
            continue
        elif k == "students":
            if len(form[k]) > 0:
                continue
            else:
                return False
        elif form[k].strip() == "" or len(form[k]) < 3:
            return False
    return True


"""
    Quart é async, bcrypt é sync e custoso, então rodamos em outra thread
    para não bloquear o event loop do Quart.
"""
def check_pw(password, hash):
    return bcrypt.checkpw(password.encode(), hash.encode())


def hash_pw(password):
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(password.encode(), salt)
