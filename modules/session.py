from quart import Blueprint, request, render_template, url_for, flash, redirect, session
from quart.utils import run_sync
from models import User, DoesNotExist
from utils import validate_form, check_pw

bp_session = Blueprint("session", __name__)


"""
    Rota de login.
"""
@bp_session.route("/login", methods=["GET", "POST"])
async def create():
    if request.method == "GET":
        return await render_template("forms/user/login.html")
    try:
        form = await request.form
        if not validate_form(form):
            await flash("Atenção, todos os campos são obrigatórios e deve conter mais de 3 caracteres.", "danger")
            return redirect(url_for("school.create"))
        user = User.get(User.username == form.get("username"))
        if await run_sync(check_pw)(form.get("password"), user.password):
            session["logged"] = True
            session["user_id"] = user.id
            await flash("Logado com sucesso.", "success")
            return redirect(url_for("user.profile"))
        else:
            await flash("Erro, usuário ou password incorreto.", "danger")
            return await render_template("forms/user/login.html")
    except DoesNotExist:
        await flash("Erro, usuário ou password incorreto.", "danger")
        return await render_template("forms/user/login.html")


"""
    Rota de logout.
"""
@bp_session.route("/logout", methods=["GET"])
async def delete():
    session.clear()
    return redirect(url_for("session.create"))
