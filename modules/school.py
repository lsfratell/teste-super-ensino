from quart import Blueprint, request, render_template, url_for, flash, redirect, g
from models import School, IntegrityError, DoesNotExist
from utils import validate_form

bp_school = Blueprint("school", __name__)


"""
    Autoriza o acesso apenas à usuários admin ao módulo School.
"""
@bp_school.before_request
async def before_request():
    if not hasattr(g, "user"):
        return redirect(url_for("session.create"))
    elif not g.user.is_admin:
        return redirect(url_for("user.profile"))


"""
    Home do usuário administrador.
"""
@bp_school.route("/", methods=["GET"])
async def index():
    q = request.args.get("q")
    sort_by = request.args.get("sort_by")
    schools = School.select()
    url = url_for('school.index')

    if q is not None:
        url += f'?q={q}'
        schools = schools.where(School.name.contains(q))

    if sort_by is not None:
        if sort_by == "desc":
            url += '&sort_by=asc' if "?" in url else '?sort_by=asc'
            schools = schools.order_by(School.name.desc())
        else:
            url += '&sort_by=desc' if "?" in url else '?sort_by=desc'
            schools = schools.order_by(School.name.asc())
    else:
        url += '&sort_by=asc' if "?" in url else '?sort_by=asc'

    return await render_template("schools.html", schools=schools, url=url)


"""
    Rota de cadastro de escolas.
"""
@bp_school.route("/create", methods=["GET", "POST"])
async def create():
    if request.method == "GET":
        return await render_template("forms/school.html")
    form = await request.form
    if not validate_form(form):
        await flash("Atenção, todos os campos são obrigatórios e deve conter mais de 3 caracteres.", "danger")
        return await render_template("forms/school.html")
    try:
        School.create(**form)
        await flash("Escola cadastrada com sucesso.", "success")
        return redirect(url_for("school.index"))
    except IntegrityError:
        await flash("Essa escola já está cadastrada.", "danger")
        return await render_template("forms/school.html")


"""
    Rota para editar/atualizar escolas.
"""
@bp_school.route("/<id>", methods=["GET", "POST"])
async def edit(id):
    try:
        school = School.get_by_id(id)
        if request.method == "GET":
            return await render_template("forms/school.html", school=school)
        form = await request.form
        if not validate_form(form):
            await flash("Atenção, todos os campos são obrigatórios e deve conter mais de 3 caracteres.", "danger")
            return await render_template("forms/school.html", school=school)
        school.name = form.get("name")
        school.director = form.get("director")
        school.address = form.get("address")
        school.students = form.get("students")
        school.save()
        await flash("Escola editada com sucesso.", "success")
        return redirect(url_for("school.index"))
    except DoesNotExist:
        await flash("Você está tentando editar/acessar uma escola que não existe.", "danger")
        return redirect(url_for("school.index"))
    except IntegrityError:
        await flash("Você está tentando editar uma escola por um nome que já existe.", "danger")
        return await render_template("forms/school.html", school=school)


"""
    Rota para deletar uma escola.
"""
@bp_school.route("/<id>/delete", methods=["GET"])
async def delete(id):
    try:
        school = School.get_by_id(id)
        school.delete_instance()
        await flash("Escola deletada com sucesso.", "success")
        return redirect(url_for("school.index"))
    except DoesNotExist:
        await flash("Você está tentando deletar uma escola que não existe.", "danger")
    return redirect(url_for("school.index"))
