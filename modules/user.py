from quart import Blueprint, request, render_template, url_for, flash, redirect, session
from quart.utils import run_sync
from models import User, IntegrityError
from utils import validate_form, hash_pw

bp_user = Blueprint("user", __name__)


"""
    Aplica restrição ao profile, apenas usuários logados.
    Seta as sessões permanentes para não ser deletada quando o usuário logado
    fechar o navegador.
"""
@bp_user.before_request
async def before_request():
    session.permanent = True

    if request.path == url_for("user.profile") and "logged" not in session.keys():
        return redirect(url_for("session.create"))


"""
    Home do usuário normal.
"""
@bp_user.route("/", methods=["GET"])
async def profile():
    return await render_template("profile.html")

"""
    Rota de cadastro de usuário.
"""
@bp_user.route("/create", methods=["GET", "POST"])
async def create():
    if request.method == "GET":
        return await render_template("forms/user/register.html")
    try:
        form = await request.form
        if not validate_form(form):
            await flash("Atenção, todos os campos são obrigatórios e deve conter mais de 3 caracteres.", "danger")
            return redirect(url_for("user.create"))
        password = await run_sync(hash_pw)(form.get("password"))
        User.create(username=form.get("username"), password=password, is_admin=form.get("is_admin") or False)
        await flash("Usuário cadastrado com sucesso.", "success")
        return redirect(url_for("session.create"))
    except IntegrityError:
        await flash(f"Esse username já existe.", "danger")
    except Exception as e:
        await flash(f"Ocorreu um erro no cadastro, tente novamente mais tarde. {e}", "danger")
    return await render_template("forms/user/register.html")
