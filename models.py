from peewee import *

db_proxy = DatabaseProxy()


class BaseModel(Model):
    class Meta:
        database = db_proxy


class School(BaseModel):
    name = CharField(null=False, unique=True, index=True)
    director = CharField(null=False)
    address = CharField(null=False)
    students = IntegerField(null=False)

    class Meta:
        table_name = "schools"


class User(BaseModel):
    username = CharField(null=False, unique=True)
    password = CharField(null=False)
    is_admin = BooleanField(null=False, default=False)

    class Meta:
        table_name = "users"
