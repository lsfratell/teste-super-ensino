import os
import pytest
import tempfile
from main import app, SqliteDatabase, db_proxy, User, School


@pytest.fixture(name="test_app", scope="module")
def _test_app():

    db = SqliteDatabase(os.path.join(tempfile.mkdtemp(), "database_test.db3"))
    db_proxy.initialize(db)

    with db:
        db.create_tables([User, School])

    return app
