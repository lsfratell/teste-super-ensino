import pytest

@pytest.mark.asyncio
async def test_not_access_profile_endpoint(test_app):
    test_client = test_app.test_client()
    resp = await test_client.get("/user/")
    assert resp.status_code == 302

@pytest.mark.asyncio
async def test_not_access_school_endpoint(test_app):
    test_client = test_app.test_client()
    resp = await test_client.get("/school/")
    assert resp.status_code == 302

@pytest.mark.asyncio
async def test_access_user_login_page(test_app):
    test_client = test_app.test_client()
    resp = await test_client.get("/session/login")
    assert resp.status_code == 200

@pytest.mark.asyncio
async def test_access_user_register_page(test_app):
    test_client = test_app.test_client()
    resp = await test_client.get("/user/create")
    assert resp.status_code == 200

@pytest.mark.asyncio
async def test_create_normal_user(test_app):
    test_client = test_app.test_client()
    resp = await test_client.post("/user/create", form={"username": "normal", "password": "normal", "is_admin": ""}, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Usuário cadastrado com sucesso." in body

@pytest.mark.asyncio
async def test_login_normal_user(test_app):
    test_client = test_app.test_client()
    resp = await test_client.post("/session/login", form={"username": "normal", "password": "normal"}, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Logado com sucesso." in body

@pytest.mark.asyncio
async def test_normal_user_access_school_module(test_app):
    test_client = test_app.test_client()
    await test_client.post("/session/login", form={"username": "normal", "password": "normal"}, follow_redirects=True)
    resp = await test_client.get("/school/")
    assert resp.status_code == 302

@pytest.mark.asyncio
async def test_create_admin_user(test_app):
    test_client = test_app.test_client()
    resp = await test_client.post("/user/create", form={"username": "admin", "password": "admin", "is_admin": "0"}, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Usuário cadastrado com sucesso." in body

@pytest.mark.asyncio
async def test_login_admin_user(test_app):
    test_client = test_app.test_client()
    resp = await test_client.post("/session/login", form={"username": "admin", "password": "admin"}, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Logado com sucesso." in body

@pytest.mark.asyncio
async def test_admin_user_access_school_module(test_app):
    test_client = test_app.test_client()
    await test_client.post("/session/login", form={"username": "admin", "password": "admin"}, follow_redirects=True)
    resp = await test_client.get("/school/")
    assert resp.status_code == 200

@pytest.mark.asyncio
async def test_admin_user_create_school(test_app):
    test_client = test_app.test_client()
    await test_client.post("/session/login", form={"username": "admin", "password": "admin"}, follow_redirects=True)
    resp = await test_client.post("/school/create", form={
        "name": "Name Escola",
        "director": "Director Director",
        "address": "Adress Address",
        "students": "123123"
    }, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Escola cadastrada com sucesso." in body
    assert "Name Escola" in body
    assert "Director Director" in body
    assert "Adress Address" in body
    assert "123123" in body

@pytest.mark.asyncio
async def test_admin_user_edit_school(test_app):
    test_client = test_app.test_client()
    await test_client.post("/session/login", form={"username": "admin", "password": "admin"}, follow_redirects=True)
    resp = await test_client.post("/school/1", form={
        "name": "Name Escola 2",
        "director": "Director Director 2",
        "address": "Adress Address 2",
        "students": "1231232"
    }, follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Escola editada com sucesso." in body
    assert "Name Escola 2" in body
    assert "Director Director 2" in body
    assert "Adress Address 2" in body
    assert "1231232" in body

@pytest.mark.asyncio
async def test_admin_user_delete_school(test_app):
    test_client = test_app.test_client()
    await test_client.post("/session/login", form={"username": "admin", "password": "admin"}, follow_redirects=True)
    resp = await test_client.get("/school/1/delete", follow_redirects=True)
    body = await resp.get_data(raw=False)
    assert resp.status_code == 200
    assert "Escola deletada com sucesso." in body
    assert "Name Escola 2" not in body
    assert "Director Director 2" not in body
    assert "Adress Address 2" not in body
    assert "1231232" not in body
